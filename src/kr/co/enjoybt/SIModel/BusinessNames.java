package kr.co.enjoybt.SIModel;

public enum BusinessNames {

	// 근린생활시설
	NEIGHBORHOOD_FACILITIES("Neighborhood Facilities"), 
	// 종교시설
	RELIGIOUS_FACILITIES("Religious Facilities"), 
	// 사회복지시설
	SOCIAL_WELFARE_FACILITIES("Social Welfare Facilities"), 
	// 의료시설
	MEDICAL_FACILITIES("Medical Facilities"), 
	// 교육연구시설
	EDUCATIONAL_RESEARCH_FACILITIES("Educational Research Facilities"), 
	// 운동시설
	EXERCISE_FACILITIES("Exercise Facilities");

	private final String value;

	BusinessNames(String value) {
	        this.value = value;
	}
	
    public final String getValue() {
        return value;
    }
}
